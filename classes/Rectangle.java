public class Rectangle extends Figure {
    private final int width = (int) (Math.random() * 20);
    private final int height = (int) (Math.random() * 20);

    public Rectangle (int centerX, int centerY) {
        super(centerX, centerY);
    }

    @Override
    public double square () {
        return width * height;
    }
}
