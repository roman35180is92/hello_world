public class Circle extends Figure {
    private final int radius = (int) (Math.random() * 10);

    public Circle (int centerX, int centerY) {
        super(centerX, centerY);
    }

    @Override
    public double square() {
        return Math.PI * radius * radius;
    }
}
