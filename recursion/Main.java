import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку");
        String string = scanner.nextLine();
        int stringLength = string.length() - 1;

        System.out.println("Рекурсивная строка: '" + recursiveString(string, stringLength) + "'");
    }

    private static String recursiveString(String string, int recursionPosition) {
        if (recursionPosition < 0) {
            return "";
        } else {
            string = string.substring(recursionPosition, recursionPosition + 1).concat(recursiveString(string,
                    recursionPosition - 1));
            return string;
        }
    }
}
