import java.util.Scanner;

public class Main {

    private static double result;

    public static void main(String[] args) {
	    System.out.println("Enter the first number");
        Scanner scanner = new Scanner(System.in);
        double a = scanner.nextDouble();

        System.out.println("Enter the second number");
        double b = scanner.nextDouble();

        System.out.println("Enter operation");
        char operation = scanner.next().charAt(0);

        boolean success = true;
        switch (operation) {
            case '+':
              result = a + b;
              break;
            case '-':
                result = a - b;
                break;
            case '*':
                result = a * b;
                break;
            case '/':
                result = a / b;
                break;
            case '%':
                result = a % b;
                break;
            default:
                success = false;
                break;
        }
        System.out.println(success ? "Result: " + result : "Error: could not determine operation");
    }
}