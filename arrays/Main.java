import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите длину массива");
        int arrayLength = scanner.nextInt();

        float[] array = new float[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            System.out.println("Введите элемент массива под номером " + i);
            array[i] = scanner.nextFloat();
        }

        String arrayString = Arrays.toString(array);
        System.out.println("\nВы ввели следующий массив:");
        System.out.println(arrayString);
        System.out.println("\nМассив, с элементами, увеличенными на 10%:");
        for (int i = 0; i < arrayLength; i++) {
            array[i] = Math.round(array[i] * 11f) / 10f;
        }
        arrayString = Arrays.toString(array);
        System.out.println(arrayString);

        float change;
        for (int i = 0; i < arrayLength; i++) {
            for (int j = arrayLength - 1; j > i; j--) {
                if (array[j] > array[j - 1]) {
                    change = array[j];
                    array[j] = array[j - 1];
                    array[j - 1] = change;
                }
            }
        }

        arrayString = Arrays.toString(array);
        System.out.println("\nМассив, в котором элементы упорядочены по убыванию:");
        System.out.println(arrayString);
    }
}