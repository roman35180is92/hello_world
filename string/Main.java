import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        String searchWord = "бяка";
        String censoredString = "[вырезано цензурой]";
        System.out.println("Введите 3 строки. Все слова '" + searchWord + "' будут заменены на '" + censoredString + "'");
	    System.out.println("Введите первую строку");
        Scanner scanner = new Scanner(System.in);
	    String firstString = scanner.nextLine().toLowerCase();
	    System.out.println("Введите вторую строку");
	    String secondString = scanner.nextLine().toLowerCase();
	    System.out.println("Введите третью строку");
	    String thirdString = scanner.nextLine().toLowerCase();
	    scanner.close();


	    System.out.println(findWord(firstString, secondString, thirdString, searchWord, censoredString));
    }

    private static String findWord(String firstString, String secondString, String thirdString, String oldWord, String newWord) {
        int randomString = (int) ((Math.random() * 2) + 1);
        switch (randomString) {
            case 1:
                return "Случайным образом была выбрана строка №1. Результат операции: " + firstString.replace(oldWord, newWord);
            case 2:
                return "Случайным образом была выбрана строка №2. Результат операции: " + secondString.replace(oldWord, newWord);
            case 3:
                return "Случайным образом была выбрана строка №3. Результат операции: " + thirdString.replace(oldWord, newWord);
            default:
                return "Ошибка вычисления случайной строки";
        }
    }
}